window.onload = function(){
    new Vue ({
        el: '#app',
        data: {
            circle: true,
            slides: [
                {
                    id: 1,
                    ativo: true,
                    tipo: 'text-emphasis',
                    titulo: 'LOREM IPSUM DOLOR?',
                    subtitulo: 'Dolor sit amet consectetur adipisicing elit',
                    image: 'image2.jpg',
                    classe: 'active'
                },
                {
                    id: 2,
                    tipo: 'text-normal',
                    titulo: 'LOREM IPSUM DOLOR?',
                    subtitulo: 'Dolor sit amet consectetur adipisicing elit',
                    image: 'image3.jpg',
                    classe: 'active'
                },
                {
                    id: 3,
                    tipo: 'text-emphasis',
                    titulo: 'LOREM IPSUM DOLOR?',
                    subtitulo: 'Dolor sit amet consectetur adipisicing elit',
                    image: 'image4.jpg',
                    classe: 'active'
                },
                {
                    id: 4,
                    tipo: 'text-emphasis',
                    titulo: 'LOREM IPSUM DOLOR?',
                    subtitulo: 'Dolor sit amet consectetur adipisicing elit',
                    image: 'image5.jpg',
                    classe: 'active'
                },
                {
                    id: 5,
                    tipo: 'text-normal',
                    titulo: 'LOREM IPSUM DOLOR?',
                    subtitulo: 'Dolor sit amet consectetur adipisicing elit',
                    image: 'image1.jpg',
                    classe: 'active'
                },
            ],
        },
        methods: {
            next:
            nextSlide = function(){
                this.slides.forEach((key, index, array) => {
                    if(index == array.length -1){
                        var removido = array.pop()
                        array.unshift(removido)
                        clearTimeout(avancar)
                    }
                });
            },
            prev:
            prevSlide = function(){
                this.slides.forEach((key, index, array)=>{
                    if(index == array.length -1){
                        var adicionado = array.shift()
                        array.push(adicionado)
                        clearTimeout(avancar)
                    }
                });
            },
            storie:
            function(){
                if(this.circle == true){
                    this.circle = false
                }else if(this.circle == false){
                    this.circle = true
                }   
            }
        },
        computed: {
            tempo:
            function() {
                this.slides.forEach((key,index,array) =>{
                    avancar = setTimeout(() => {
                        if(index == array.length -1){
                            var removido = array.pop()
                            array.unshift(removido)
                            clearTimeout(avancar)
                        }      
                    }, 5000);
                })
            },
        },

    })
}